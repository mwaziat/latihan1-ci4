<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Dashboard - Voler Admin Dashboard</title>

    <link rel="stylesheet" href="<?= base_url(); ?>/assets/backend/css/bootstrap.css">

    <link rel="stylesheet" href="<?= base_url(); ?>/assets/backend/vendors/chartjs/Chart.min.css">

    <link rel="stylesheet" href="<?= base_url(); ?>/assets/backend/vendors/perfect-scrollbar/perfect-scrollbar.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/backend/css/app.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/plugins/jquery/jquery-ui.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/plugins/datatables/dataTables.bootstrap4.min.css">
    <link rel="stylesheet" href="<?= base_url(); ?>/assets/plugins/fontawesome-free\css\all.min.css">
    <link rel="shortcut icon" href="<?= base_url(); ?>/assets/backend/images/favicon.svg" type="image/x-icon">

    <style>
        .title-sidebar {
            font-size: 14px !important;
        }
    </style>
</head>

<body>
    <div id="app">
        <?= view('Backend/sidebar'); ?>
        <div id="main">
            <?= view('Backend/topbar'); ?>
            <?= $this->renderSection('content') ?>
            <footer>
                <div class="footer clearfix mb-0 text-muted">
                    <div class="float-end">
                        <p>2020 &copy; MZIAD.COM</p>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="<?= base_url(); ?>/assets/plugins/jquery/jquery.min.js"></script>
    <script src="https://code.jquery.com/ui/1.12.0/jquery-ui.js" integrity="sha256-0YPKAwZP7Mp3ALMRVB2i8GXeEndvCq3eSl/WsAl1Ryk=" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <!-- DataTables -->
    <script src="<?php echo base_url() ?>/assets/plugins/datatables/jquery.dataTables.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/datatables/dataTables.bootstrap4.js"></script>
    <script src="<?php echo base_url() ?>/assets/plugins/fontawesome-free\js\all.min.js"></script>
    <script src="<?= base_url(); ?>/assets/backend/js/feather-icons/feather.min.js"></script>
    <script src="<?= base_url(); ?>/assets/backend/vendors/perfect-scrollbar/perfect-scrollbar.min.js"></script>
    <script src="<?= base_url(); ?>/assets/backend/js/app.js"></script>

    <script src="<?= base_url(); ?>/assets/backend/js/main.js"></script>
    <?= $this->renderSection('script') ?>
</body>

</html>