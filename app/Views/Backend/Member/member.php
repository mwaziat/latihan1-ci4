<?= $this->extend('Backend\layout') ?>

<?= $this->section('content') ?>
<div class="main-content container-fluid">
    <div class="page-title">
        <div class="row">
            <div class="col-12 col-md-6 order-md-1 order-last">
                <h3>List</h3>
            </div>
            <div class="col-12 col-md-6 order-md-2 order-first">
                <nav aria-label="breadcrumb" class='breadcrumb-header'>
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="index.html">Dashboard</a></li>
                        <li class="breadcrumb-item active" aria-current="page">Anggota</li>
                    </ol>
                </nav>
            </div>
        </div>

    </div>
    <!-- Basic Tables start -->
    <div class="row mt-3" id="basic-table">
        <div class="col-12 col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between">
                        <h4 class="card-title">Table with outer spacing</h4>
                        <!-- Button trigger for Extra Large  modal -->
                        <button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#addMember">
                            Tambah Member
                        </button>
                    </div>
                </div>
                <div class="card-body">
                    <!-- Table with outer spacing -->
                    <div class="table-responsive">
                        <table class="table" id="member">
                            <thead>
                                <tr>
                                    <th>Nama Lengkap</th>
                                    <th>NTA</th>
                                    <th>Kelas</th>
                                    <th>Jabatan</th>
                                    <th>Alamat</th>
                                    <th>Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="text-bold-500">Michael Right</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">
                                        <a href="" class="btn btn-sm btn-warning"><i data-feather="edit"></i> Edit</a>
                                        <a href="" class="btn btn-sm btn-danger"><i data-feather="trash-2"></i> Hapus</a>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text-bold-500">Morgan Vanblum</td>
                                    <td>$13/hr</td>
                                    <td class="text-bold-500">Graphic concepts</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">
                                        <a href="" class="btn btn-sm btn-warning"><i data-feather="edit"></i> Edit</a>
                                        <a href="" class="btn btn-sm btn-danger"><i data-feather="trash-2"></i> Hapus</a>
                                    </td>

                                </tr>
                                <tr>
                                    <td class="text-bold-500">Tiffani Blogz</td>
                                    <td>$15/hr</td>
                                    <td class="text-bold-500">Animation</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">UI/UX</td>
                                    <td class="text-bold-500">
                                        <a href="" class="btn btn-sm btn-warning"><i data-feather="edit"></i> Edit</a>
                                        <a href="" class="btn btn-sm btn-danger"><i data-feather="trash-2"></i> Hapus</a>
                                    </td>

                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="me-1 mb-1 d-inline-block">
    <!--Extra Large Modal -->
    <div class="modal fade text-left w-100 modal-borderless" id="addMember" tabindex="-1" role="dialog" aria-labelledby="myModalLabel16" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered modal-dialog-scrollable modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel16">Extra Large
                        Modal</h4>
                    <button type="button" class="close" data-bs-dismiss="modal" aria-label="Close">
                        <i data-feather="x"></i>
                    </button>
                </div>
                <div class="modal-body">
                    <form method="post" action="">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="fullName">Nama Lengkap</label>
                                    <input type="text" name="fullName" class="form-control round" id="fullName" placeholder="Nama Lengkap">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nickName">Nama Panggilan</label>
                                    <input type="text" name="nickName" class="form-control round" id="nickName" placeholder="Nama Panggilan">
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-light-secondary" data-bs-dismiss="modal">
                        <i class="bx bx-x d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Close</span>
                    </button>
                    <button type="button" class="btn btn-primary ml-1" data-bs-dismiss="modal">
                        <i class="bx bx-check d-block d-sm-none"></i>
                        <span class="d-none d-sm-block">Accept</span>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<?= $this->endSection() ?>
<?= $this->section('script') ?>

<script>
    $(document).ready(function() {
        $("#member").DataTable();
    });
</script>
<?= $this->endSection() ?>