<?= $this->extend('Auth\layout') ?>

<?= $this->section('content') ?>
<form action="/login/auth" method="post">
    <div class="form-group text-center">
        <label for="code">Masukkan kode yang di dapatkan</label>
        <input type="hidden" name="email" id="email" value="<?= $email; ?>">
        <div class="row">
            <div class="col">
                <input type="text" class="form-control" name="code1" id="code1" max="1">
            </div>
            <div class="col">
                <input type="text" class="form-control" name="code2" id="code2" max="1">
            </div>
            <div class="col">
                <input type="text" class="form-control" name="code3" id="code3" max="1">
            </div>
            <div class="col">
                <input type="text" class="form-control" name="code4" id="code4" max="1">
            </div>
        </div>
    </div>
    <div class="form-group text-center">
        <small id="msgError" style="display: none;"></small>
        <small id="counterDown"> Waktu tersisa :<p id="time"></p></small>
    </div>
</form>
<?= $this->endSection() ?>
<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        $("#code1, #code2, #code3, #code4").keypress(function(data) {
            if (data.which != 8 && data.which != 0 && (data.which < 48 || data.which > 57)) {
                $('#msgError').addClass('text-danger').html('Hanya boleh input angka').css('display', 'block');
                return false;
            }

        });
        $("#code1").keyup(function(data) {
            var val1 = $('#code1').val();
            if (val1.length == 1) {
                $('#code2').focus();
            }
        });
        $("#code2").keyup(function(data) {
            var val2 = $('#code2').val();
            if (val2.length == 1) {
                $('#code3').focus();
            }
            if (val2.length == 0) {
                $('#code1').val('').focus();
                $('#code2').val('');
            }
        });
        $("#code3").keyup(function(data) {
            var val3 = $('#code3').val();
            if (val3.length == 1) {
                $('#code4').focus();
            }
            if (val3.length == 0) {
                $('#code1').val('').focus();
                $('#code2').val('');
                $('#code3').val('');
            }
        });
        $("#code4").keyup(function(data) {
            var val4 = $('#code4').val();
            var val1 = $('#code1').val();
            var val2 = $('#code2').val();
            var val3 = $('#code3').val();
            if (val4.length == 0) {
                $('#code1').val('').focus();
                $('#code2').val('');
                $('#code3').val('');
                $('#code4').val('');

            }
            if (val1 && val2 && val3 && val4 != '') {
                var val = String(val1) + String(val2) + String(val3) + String(val4);
                var email = $('#email').val();
                $.ajax({
                    type: "POST",
                    url: "/register/varifyCode",
                    data: {
                        code: val,
                        email: email
                    },
                    dataType: "JSON",
                    success: function(response) {
                        if (response.response == 'success') {
                            Swal.fire({
                                icon: 'success',
                                title: 'Berhasil',
                                text: response.msg,
                            }).then(function(result) {
                                if (result.isConfirmed) {
                                    const expires = new Date();
                                    setCookieErr("succVal", 'succVal', expires.getMinutes() + 1);
                                    setInterval(function() {
                                        setCookie("dataTime", '', 1000);
                                    }, 1000);
                                    location.href = "/login";
                                }
                            });
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Ada yang salah!',
                                text: response.msg,
                            });
                        }
                    }
                });
            }
        });

    });

    function startTimer(duration, display) {
        var start = Date.now(),
            diff,
            minutes,
            seconds;
        const expires = new Date();

        function timer() {
            diff = duration - (((Date.now() - start) / 1000) | 0);
            minutes = (diff / 60) | 0;
            seconds = (diff % 60) | 0;

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.textContent = minutes + ":" + seconds;

            if (diff <= 0) {
                start = Date.now() + 1000;
            }

            if (diff == 0) {
                setCookieErr("sess", 'exp', expires.getMinutes() + 1);
                window.location.href = window.location.origin + '/login';
                clearInterval(timer);
            }
            // console.log(expires.getMinutes() + 1);
        };
        // we don't want to wait a full second before the timer starts
        timer();
        setInterval(function() {
            timer();
        }, 1000);

        setInterval(function() {
            setCookie("dataTime", minutes, 30000);
        }, 25000);
    }

    function setCookie(name, value, time) {
        document.cookie = name + "=" + value + ";" + time;
    }

    function setCookieErr(name, value, time) {
        document.cookie = name + "=" + value + ";" + time + ";path=/";
    }

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    window.onload = function() {
        var minutes_a = getCookie("dataTime");
        if (minutes_a != '') {
            var fiveMinutes = 60 * minutes_a + 1;
        }

        if (minutes_a == 00) {
            var fiveMinutes = 60 * 10;
        }
        var display = document.querySelector('#time');
        startTimer(fiveMinutes, display);
    };
</script>
<?= $this->endSection() ?>