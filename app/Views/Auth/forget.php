<?= $this->extend('Auth\layout') ?>

<?= $this->section('content') ?>
<?php if (session()->getFlashdata('msg')) : ?>
    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
<?php endif; ?>

<form action="/login/forget" method="post">
    <?= csrf_field(); ?>
    <div class="form-group">
        <label for="email">Email / NTA</label>
        <input type="text" name="id" class="form-control" id="email" placeholder="email / nta">
        <small id="msgEmail"></small>
    </div>
    <div id="changePass" style="display: none;">
        <div class="form-group">
            <label for="exampleInputPassword1">Password Baru</label>
            <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Password">
        </div>
        <div class="form-group">
            <label for="exampleInputPassword2">Konfirmasi Password Baru</label>
            <input type="password" name="passconf" class="form-control" id="exampleInputPassword2" placeholder="Konfirmasi Password">
        </div>
    </div>
    <div class="d-flex justify-content-between mt-2">
        <a href="/login"> Login >></a>
        <button type="submit" class="btn btn-sm btn-primary-1" id='btnSubmit'>Forget</button>
    </div>
</form>
<?= $this->endSection() ?>
<?= $this->section('script') ?>

<script>
    $(document).ready(function() {
        $('#changePass').hide();
        $('#btnSubmit').attr('disabled', 'disabled');
        $('#email').keyup(function() {
            var val = $('#email').val();
            if (IsEmail(val) == false) {
                $('#changePass').hide();
                $('#btnSubmit').attr('disabled', 'disabled');
                $('#msgEmail').show().html('Email / NTA tidak valid, periksa kembali email/nta anda').addClass('text-danger');
            } else {
                $.ajax({
                    type: "POST",
                    url: "/change_password",
                    data: {
                        data: val
                    },
                    dataType: "JSON",
                    success: function(response) {
                        if (response == true) {
                            $('#msgEmail').hide().html('');
                            $('#changePass').show();
                            $('#btnSubmit').removeAttr('disabled');
                        } else {
                            $('#changePass').hide();
                            $('#btnSubmit').attr('disabled', 'disabled');
                            $('#msgEmail').show().html('Akun anda tidak ditemukan, periksa kembali email/nta anda').addClass('text-danger');
                        }
                    }
                });
            }
        });

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }
    });
</script>

<?= $this->endSection() ?>