<?= $this->extend('Auth\layout') ?>

<?= $this->section('content') ?>
<?php if (session()->getFlashdata('msg')) : ?>
    <div class="alert alert-danger"><?= session()->getFlashdata('msg') ?></div>
<?php elseif (session()->getFlashdata('success')) : ?>
    <div class="alert alert-success"><?= session()->getFlashdata('success') ?></div>
<?php endif; ?>

<div id="msgExp" style="display: none;">
</div>

<form action="/login/auth" method="post">
    <div class="form-group">
        <label for="email">Email / NTA</label>
        <input type="text" name="id" class="form-control" id="email" placeholder="email / nta">
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Password">
    </div>
    <div class="d-flex justify-content-between mt-2">
        <div class="form-check">
            <input type="checkbox" class="form-check-input" id="exampleCheck1">
            <label class="form-check-label" for="exampleCheck1">Ingatkan Saya</label>
        </div>

        <button type="submit" class="btn btn-sm btn-primary-1">Login</button>
    </div>
    <div class="d-flex mt-3">
        <a href="/change_password"> Forget Password >></a>
    </div>
</form>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        setTimeout(() => {
            var exp = getCookie("sess");
            var succVal = getCookie("succVal");
            if (exp != '') {
                $('#msgExp').html('<div class="alert alert-danger">Waktu anda sudah habis harap periksa kembali</div>').css('display', 'block');
            }
            if (succVal != '') {
                $('#msgExp').html('<div class="alert alert-success">Berhasil verifikasi akun, silahkan login</div>').css('display', 'block');
            }
            deleteCookie("sess")
            deleteCookie("succVal")
        }, 1000);

        setTimeout(() => {
            $('#msgExp').html('').css('display', 'none');
        }, 5000);
    });

    function getCookie(cname) {
        var name = cname + "=";
        var decodedCookie = decodeURIComponent(document.cookie);
        var ca = decodedCookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') {
                c = c.substring(1);
            }
            if (c.indexOf(name) == 0) {
                return c.substring(name.length, c.length);
            }
        }
        return "";
    }

    function deleteCookie(name) {
        var d = new Date();
        var expires = d.getMinutes() + 1
        document.cookie = name + "=;" + expires;
    }
</script>
<?= $this->endSection() ?>