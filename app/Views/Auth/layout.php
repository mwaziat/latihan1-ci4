<!doctype html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="icon" href="<?php echo base_url() ?>/assets\img\lg-1.png">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/plugins/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="<?php echo base_url() ?>/assets/plugins/sweetalert/dist/sweetalert2.min.css">

    <title>PRAMUKA SMANSABA</title>
    <style>
        body,
        html {
            height: 100%;
        }

        .bg-1 {
            background-color: #E8F6EF !important;
        }

        .bg-2 {
            background-color: #F9F9F9 !important;
        }

        .btn-warning-1 {
            background-color: #FFE194 !important;
        }

        .btn-success-1 {
            background-color: #B8DFD8 !important;
        }

        .btn-disabled-1 {
            background-color: #4C4C6D !important;
        }

        .btn-primary-1 {
            background-color: #39A2DB !important;
        }

        .btn-info-1 {
            background-color: #A2DBFA !important;
        }

        .rounded-10 {
            border-radius: 10px !important;
        }

        .rounded-15 {
            border-radius: 15px !important;
        }

        .rounded-20 {
            border-radius: 20px !important;
        }

        img.img-logo {
            border: 3px solid #FFE194;
            padding: 5px;
        }

        .d-grid {
            display: grid !important;
        }

        .nav-item:hover {
            border-bottom: 2px solid #FFE194;
        }
    </style>
</head>
<?php $request = \Config\Services::request(); ?>

<body style="background-color: #E8F6EF;">
    <nav class="navbar navbar-expand-lg navbar-light bg-light fixed-top pt-3 bg-1">
        <div class="container">
            <a class="navbar-brand ml-2 font-weight-bold" href="#">PRAMUKA SMANSABA</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse text-center" id="navbarSupportedContent">
                <ul class="navbar-nav ml-auto font-weight-bold">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= base_url(); ?>">Home <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Berita</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Galeri</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="#">Alumni</a>
                    </li>
                </ul>
                <?php
                // $request = \Config\Services::request();
                if ($request->uri->getSegment(1) == 'register') {
                    echo "";
                } else { ?>
                    <a href="/register" class="btn btn-sm btn-outline-danger ml-4">Daftar</a>
                <?php }
                ?>
            </div>
        </div>
    </nav>
    <div class="container h-100">
        <div class="row h-100 justify-content-md-center align-items-center">
            <?php
            if ($request->uri->getSegment(1) == 'register' && $request->uri->getSegment(2) == 'verify') { ?>
                <div class="col-lg-8">
                <?php } else if ($request->uri->getSegment(1) == 'register') { ?>
                    <div class="col-lg-12">
                    <?php } else { ?>
                        <div class="col-lg-8">
                        <?php }
                        ?>
                        <div class="card p-5 bg-2 rounded-15">
                            <div class="row p-2">
                                <div class="col-lg-6 p-3 mt-2 align-self-center">
                                    <div class="text-center">
                                        <img src="<?php echo base_url() ?>/assets\img\lg-1.png" class="img-fluid" height="200" width="200" alt="...">
                                    </div>
                                </div>
                                <div class="col-lg-6 p-3 mt-2 align-self-center">
                                    <?= $this->renderSection('content') ?>
                                </div>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
                <!-- Optional JavaScript -->
                <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                <script src="<?php echo base_url() ?>/assets/plugins/jquery/jquery.min.js"></script>
                <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
                <script src="<?php echo base_url() ?>/assets/plugins/bootstrap/js/bootstrap.min.js"></script>
                <script src="<?php echo base_url() ?>/assets/plugins/sweetalert/dist/sweetalert2.min.js"></script>
                <?= $this->renderSection('script') ?>
</body>

</html>