<?= $this->extend('Auth\layout') ?>

<?= $this->section('content') ?>
<?= $validation->listErrors() ?>
<?php if (session()->getFlashdata('success')) : ?>
    <div class="alert alert-success"><?= session()->getFlashdata('msg') ?></div>
<?php elseif (session()->getFlashdata('error')) : ?>
    <div class="alert alert-error"><?= session()->getFlashdata('msg') ?></div>
<?php endif; ?>

<form action="/register" method="post" id="formRegister">
    <?= csrf_field(); ?>
    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label for="fullName">Nama Lengkap</label>
                <input type="text" name="fullName" class="form-control" id="fullName" placeholder="Nama Lengkap">
                <small id="fullNameMsg" class="form-text"></small>
            </div>
        </div>
        <div class="col-md-6">
            <div class="form-group">
                <label for="nickName">Nama Panggilan</label>
                <input type="text" name="nickName" class="form-control" id="nickName" placeholder="Nama Panggilan">
                <small id="nickNameMsg" class="form-text"></small>
            </div>
        </div>
    </div>
    <div class="form-group">
        <label for="phone">Telepon / Hp</label>
        <input type="text" name="phone" class="form-control" id="phone" placeholder="Telepon / Hp">
        <small id="phoneMsg" class="form-text"></small>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" name="email" class="form-control" id="email" placeholder="Email">
        <small id="emailMsg" class="form-text"></small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword1">Password</label>
        <input type="password" name="pass" class="form-control" id="exampleInputPassword1" placeholder="Password">
        <small id="passMsg" class="form-text"></small>
    </div>
    <div class="form-group">
        <label for="exampleInputPassword2">Konfirmasi Password</label>
        <input type="password" name="passconf" class="form-control" id="exampleInputPassword2" placeholder="Konfirmasi Password">
        <small id="passconfMsg" class="form-text"></small>
    </div>
    <div class="d-flex justify-content-between mt-2">
        <a href="/login"> Login >></a>
        <button type="submit" class="btn btn-sm btn-primary-1" id="register">Register</button>
    </div>
</form>
<?= $this->endSection() ?>

<?= $this->section('script') ?>
<script>
    $(document).ready(function() {
        $("#phone").keypress(function(data) {
            if (data.which != 8 && data.which != 0 && (data.which < 48 || data.which > 57)) {
                $('#phoneMsg').addClass('text-danger').html('Telpon / Hp hanya berupa angka');
                return false;
            }
        });

        $('#exampleInputPassword1, #exampleInputPassword2').keyup(function() {
            var pass = $('#exampleInputPassword1').val();
            var passconf = $('#exampleInputPassword2').val();
            if (pass.length < 5) {
                $('#passMsg').addClass('text-danger').html('Password tidak boleh kurang 5 karakter');
            } else {
                $('#passMsg').removeClass('text-danger').html('');
            }
            if (passconf.length < 5) {
                $('#passconfMsg').addClass('text-danger').html('Konfirmasi password tidak boleh kurang 5 karakter');
            } else {
                if (pass != passconf) {
                    $('#passconfMsg').addClass('text-danger').html('Password tidak sama');
                } else {
                    $('#passconfMsg').removeClass('text-danger').addClass('text-success').html('Kombinasi password baik');
                    // $('#passconfMsg').removeClass('text-danger').html('');
                }
            }
        });

        function IsEmail(email) {
            var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
            if (!regex.test(email)) {
                return false;
            } else {
                return true;
            }
        }

        $('#formRegister').submit(function(e) {
            e.preventDefault();

            var fullName = $('#fullName').val();
            var nickName = $('#nickName').val();
            var phone = $('#phone').val();
            var email = $('#email').val();
            var pass = $('#exampleInputPassword1').val();
            var passconf = $('#exampleInputPassword2').val();

            if (fullName == '') {
                $('#fullNameMsg').addClass('text-danger').html('Nama lengkap tidak boleh kosong');
            } else {
                $('#fullNameMsg').removeClass('text-danger').html('');
            }
            if (nickName == '') {
                $('#nickNameMsg').addClass('text-danger').html('Nama panggilan tidak boleh kosong');
            } else {
                $('#nickNameMsg').removeClass('text-danger').html('');
            }
            if (phone == '') {
                $('#phoneMsg').addClass('text-danger').html('Telpon / Hp tidak boleh kosong');
            } else {
                if (phone.length < 8) {
                    $('#phoneMsg').addClass('text-danger').html('Telpon / Hp tidak kurang dari 8 karakter');
                } else {
                    $('#phoneMsg').removeClass('text-danger').html('');
                }
            }
            if (email == '') {
                $('#emailMsg').addClass('text-danger').html('Email tidak boleh kosong');
            } else {
                if (IsEmail(email) == false) {
                    $('#emailMsg').addClass('text-danger').html('Format email tidak benar');
                } else {
                    $('#emailMsg').removeClass('text-danger').html('');
                }
            }
            if (pass == '') {
                $('#passMsg').addClass('text-danger').html('Password tidak boleh kosong');
            } else {
                $('#passMsg').removeClass('text-danger').html('');
            }
            if (passconf == '') {
                $('#passconfMsg').addClass('text-danger').html('Konfirmasi password tidak boleh kosong');
            } else {
                $('#passconfMsg').removeClass('text-danger').html('');
            }

            if (fullName && nickName && email && phone && pass && passconf != '') {
                $.ajax({
                    type: "Post",
                    url: "/register",
                    data: $('#formRegister').serialize(),
                    dataType: "JSON",
                    success: function(response) {
                        if (response.response == 'success') {
                            Swal.fire({
                                icon: 'success',
                                title: 'Berhasil',
                                text: response.msg,
                            }).then(function(result) {
                                if (result.isConfirmed) {
                                    location.reload();
                                }
                            });
                        } else if (response.response == 'errorEmail') {
                            $('#emailMsg').addClass('text-danger').html(response.msg);
                        } else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Ada yang salah!',
                                text: response.msg,
                            });
                        }
                    }
                });
            }

        });

    });
</script>
<?= $this->endSection() ?>