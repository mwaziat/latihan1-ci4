<?php

namespace App\Controllers\Auth;

use App\Controllers\BaseController;
use App\Models\Auth;
use App\Models\UsersModel;
use App\Models\TokenModel;
use App\Libraries\Mail;

class Login extends BaseController
{
	public $login;
	public $user;
	public $token;
	public $email;
	public function __construct()
	{
		helper(['form']);
		$this->login = new Auth();
		$this->user = new UsersModel();
		$this->token = new TokenModel();
		$this->email = new Mail();
	}

	public function index()
	{
		if (!session()->get('logged_in')) {
			helper(['form']);
			return view('Auth/login');
		} else {
			return redirect()->to('/dashboard');
		}
	}

	public function auth()
	{
		$session = session();
		$model = new Auth();

		$id = $this->request->getPost('id');
		$password = $this->request->getPost('pass');

		$loginData = $model->where('email', $id)->first();
		if ($loginData) {
			$passData = $loginData['password'];
			$verify_pass = password_verify($password, $passData);
			if ($verify_pass) {
				if ($loginData['status'] == 1) {
					$sess = [
						'login_id' => $loginData['login_id'],
						'email' => $loginData['email'],
						'role' => $loginData['role'],
						'status' => $loginData['status'],
						'logged_in' => true
					];
					$session->set($sess);
					return redirect()->to('/dashboard');
				} else {
					$session->setFlashdata('msg', 'Akun anda belum active, segera cek email anda untuk activasi akun');
					return redirect()->to('/login');
				}
			} else {
				$session->setFlashdata('msg', 'Password Salah');
				return redirect()->to('/login');
			}
		} else {
			$session->setFlashdata('msg', 'Email / NTA Salah');
			return redirect()->to('/login');
		}
	}

	public function register()
	{
		session();
		$data = [
			'validation' => \Config\Services::validation()
		];
		return view('Auth/register', $data);
	}

	public function created()
	{
		if ($this->request->isAJAX()) {
			$email = htmlspecialchars($this->request->getVar('email'), ENT_QUOTES);
			$login = new Auth();
			$loginData = $login->where('email', $email)->first();

			if (!$loginData) {
				$dataLogin = [
					'email' => $email,
					'password' => password_hash($this->request->getVar('passconf'), PASSWORD_DEFAULT),
					'role_id' => 2,
					'status' => 0
				];
				$login->insertData($dataLogin);
				$login_id = $login->insertID();

				$user = new UsersModel();
				$nta = $user->getLastNta();

				$explodeNta = explode("-", $nta['NTA']);
				$NoAnggota = (int) $explodeNta[2];
				$NoAnggota++;
				$defaultNta = 'default-' . rand(0, 500) . '-';

				$ntaCode = $defaultNta . sprintf("%03s", $NoAnggota);

				$userData = [
					'login_id' => $login_id,
					'nta' => $ntaCode,
					'full_name' => $this->request->getVar('fullName'),
					'nick_name' => $this->request->getVar('nickName'),
					'phone' => $this->request->getVar('phone')
				];
				$user->save($userData);


				$token = new TokenModel();
				$stringToken = base64_encode(random_bytes(32));
				$tokenLink = preg_replace("/[^a-zA-Z]/", "", $stringToken);
				$randCode = rand(1000, 9999);
				$tokenData = [
					'email' => $email,
					'token_link' => $tokenLink,
					'token_code' => $randCode,
					'token_expire' => time(),
					'status' => 'active',
				];
				$createToken = $token->save($tokenData);
				if ($createToken) {
					$sendFrom = 'myddmin@gmail.com';
					$sendTo = $email;
					$senderName = 'PRAMUKA SMANSABA';
					$sendMsg = '';
					$sendMsg .= '<h1>Pendaftaran Berhasil</h1> <h3>Segera lakukan aktifasi akun anda.</h3>';
					$sendMsg .= '<p><b>Silahkan klik link berikut dan masukkan code verifikasi dibawah</b></p><br>';
					$sendMsg .= '<a href="' . base_url() . '/register/verify?email=' . base64_encode($email) . '&token=' . $tokenLink . '&code=' . base64_encode($randCode) . '">My Token</a>';
					$sendMsg .= '<p>Copy kode berikut: <b>' . $randCode . '</b></p>';
					$email = new Mail();
					$sendingMsg = $email->sendMail($sendFrom, $sendTo, $senderName, $sendMsg);
					if ($sendingMsg) {
						$jsonData = [
							'response' => 'success',
							'msg' => 'Berhasil melakukan registrasi, cek email anda.'
						];
					} else {
						$jsonData = [
							'response' => 'error',
							'msg' => 'Tidak dapat mengirimkan token.'
						];
					}
				} else {
					$jsonData = [
						'response' => 'error',
						'msg' => 'Tidak dapat membuat token.'
					];
				}
			} else {
				$jsonData = [
					'response' => 'errorEmail',
					'msg' => 'Email sudah ada, coba yang lain.'
				];
			}
		} else {
			$jsonData = [
				'response' => 'error',
				'msg' => 'Ajax is request, can\'t be access this page.'
			];
		}

		return $this->response->setJSON($jsonData);
	}

	public function changePassword()
	{
		return view('Auth/forget');
	}

	public function forget()
	{
		$rules = [
			'pass'      => 'required|min_length[6]|max_length[200]',
			'passconf'  => 'matches[pass]'
		];

		$session = session();
		if ($this->validate($rules)) {

			$id = $this->request->getPost('id');
			$password = $this->request->getPost('passconf');

			$loginData = $this->login->where('email', $id)->first();
			if ($loginData) {
				if ($loginData['status'] == 1) {
					$data = [
						'email' => $id,
						'password' => password_hash($password, PASSWORD_DEFAULT)
					];
					$update = $this->login->updateLogin($data, $id);
					if ($update == true) {
						$session->setFlashdata('success', 'Berhasil mengganti password');
						return redirect()->to('/login');
					} else {
						$session->setFlashdata('msg', 'Tidak dapat merubah password');
						return redirect()->to('/change_password');
					}
				} else {
					$session->setFlashdata('msg', 'Anda tidak bisa mengganti password. Segera verifikasi akun anda terlebih dahulu');
					return redirect()->to('/change_password');
				}
			} else {
				$session->setFlashdata('msg', 'Email / NTA Salah');
				return redirect()->to('/change_password');
			}
		} else {
			$session->setFlashdata('msg', 'Password tidak sesuai atau kosong');
			return redirect()->to('/change_password');
		}
	}

	public function checkEmail()
	{
		$email = $this->request->getVar('data');
		$loginData = $this->login->where('email', $email)->first();
		if ($loginData) {
			if ($loginData['status'] == 1) {
				$jsonData = true;
			} else {
				$jsonData = false;
			}
		} else {
			$jsonData = false;
		}
		return $this->response->setJSON($jsonData);
	}

	public function verify()
	{
		$email = base64_decode($this->request->getVar('email'));
		$code =  base64_decode($this->request->getVar('code'));
		$token =  $this->request->getVar('token');

		$dataToken = $this->token->validateToken($email, $code);

		if ($dataToken) {
			if ($token == $dataToken['token_link']) {

				if (time() - $dataToken['token_expire'] < (60 * 60 * 72)) {
					$dataToken = ['satus' => 'pending'];
					$this->token->updateToken($dataToken, $email);

					$data['email'] = base64_encode($email);
					return view('Auth/verify', $data);
				} else {
					$dataLogin = $this->login->where('email', $email)->first();
					$dataUser = $this->user->where('login_id', $dataLogin['login_id'])->first();
					if ($dataUser > 0) {
						$this->user->deleteUser($dataLogin['login_id']);
						$this->login->deleteLogin_email($email);
						$this->token->deleteToken($email);
					} else {
						$this->session->setFlashdata('msg', 'Tidak ada data tersedia');
						return redirect()->to('/login');
					}
				}
			} else {
				throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
			}
		} else {
			throw \CodeIgniter\Exceptions\PageNotFoundException::forPageNotFound();
		}
	}

	public function varifyCode()
	{
		$email = base64_decode($this->request->getVar('email'));
		$code = $this->request->getVar('code');

		$dataToken = $this->token->validateToken($email, $code);
		if ($dataToken) {
			$dataLogin = $this->login->where('email', $email)->first();
			if ($dataLogin) {
				$data = [
					'email' => $email,
					'status' => 1
				];
				$update = $this->login->updateLogin($data, $email);
				if ($update) {
					$this->token->deleteToken($email);
					$jsonData = [
						'response' => 'success',
						'msg' => 'Berhasil melakukan verifikasi data, silahkan login.'
					];
				} else {
					$jsonData = [
						'response' => 'error',
						'msg' => 'Tidak dapat melakukan perubahan, harap pastikan kode yang anda masukkan.'
					];
				}
			} else {
				$jsonData = [
					'response' => 'error',
					'msg' => 'Akun tidak dapat ditemukan, harap pastikan kode yang anda masukkan.'
				];
			}
		} else {
			$jsonData = [
				'response' => 'error',
				'msg' => 'Token tidak ditemukan, harap pastikan kode yang anda masukkan.'
			];
		}
		return $this->response->setJSON($jsonData);
	}

	public function logout()
	{
		$session = session();
		$session->destroy();
		return redirect()->to('/login');
	}
}
