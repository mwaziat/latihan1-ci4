<?php

namespace App\Controllers\Backend\Dashboard;

use App\Controllers\BaseController;

class DashboardController extends BaseController
{
	public function index()
	{
		echo view('Backend/dashboard/dashboard');
	}
}
