<?php

namespace App\Libraries;

class Mail
{
    /**
     * Send From you can set string type
     * @var string $sendFrom
     * 
     * Send to must be array type
     * @var array $sendTo
     * 
     * Sender name you can set string type
     * @var string $senderName
     * 
     * Send CC and Bcc must be array type
     * @var array $sendCc 
     * @var array $sendBcc
     * 
     * Send subject can be set string type
     * @var string $subject
     * 
     */

    public function sendMail($sendFrom, $sendTo,  $senderName, $sendMsg, $sendCc = null, $sendBcc = null, $subject = null, $file = null)
    {
        $email = \Config\Services::email();
        $email->setFrom($sendFrom, $senderName);
        $email->setTo($sendTo);

        if ($sendCc != null) {
            $email->setCC($sendCc);
        }

        if ($sendBcc != null) {
            $email->setBCC($sendBcc);
        }

        $email->setSubject($subject);
        $email->setMessage($sendMsg);

        if ($file != null) {
            $email->attach($file);
        }
        if (!$email->send()) {
            return false;
        } else {
            return true;
        }
    }
}
