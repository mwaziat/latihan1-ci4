<?php

namespace Config;



// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php')) {
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Home');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/*
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Home::index');

$routes->get('/login', 'Auth\login::index');
$routes->post('/login/auth', 'Auth\login::auth');
$routes->get('/logout', 'Auth\login::logout');
$routes->get('/change_password', 'Auth\login::changePassword');
$routes->post('/change_password', 'Auth\login::checkEmail');
$routes->add('/login/forget', 'Auth\login::forget');
$routes->get('/register', 'Auth\login::register');
$routes->add('/register', 'Auth\login::created');
$routes->get('/register/verify?(:any)', 'Auth\login::verify');
$routes->post('/register/varifyCode', 'Auth\login::varifyCode');

$routes->get('/dashboard', 'Backend\Dashboard\DashboardController::index', ['filter' => 'auth']);
$routes->get('/member', 'Backend\Member\UsersController::index');

/*
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php')) {
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
