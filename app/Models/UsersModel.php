<?php

namespace App\Models;

use CodeIgniter\Model;

class UsersModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'tb_users';
	protected $primaryKey           = 'user_id';
	protected $useAutoIncrement     = true;
	protected $insertID             = 0;
	protected $returnType           = 'array';
	protected $useSoftDelete        = false;
	protected $protectFields        = true;
	protected $allowedFields        = ['login_id', 'nta', 'full_name', 'nick_name', 'birthday', 'address', 'phone'];

	public function getLastNta()
	{
		$query = $this->db->query("SELECT nta as NTA FROM tb_users ORDER BY user_id DESC LIMIT 1");
		return $query->getRowArray();
	}

	public function deleteUser($login_id)
	{
		$query  = $this->db->table($this->table);
		$query->where('login_id', $login_id);
		return $query->delete();
	}

	
}
