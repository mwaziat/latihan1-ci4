<?php

namespace App\Models;

use CodeIgniter\Model;

class TokenModel extends Model
{
	protected $DBGroup              = 'default';
	protected $table                = 'tb_token';
	protected $allowedFields        = ['email', 'token_link', 'token_code', 'token_expire', 'status'];

	public function validateToken($email, $code)
	{
		$query  = $this->db->table($this->table);
		$query->where('email', $email);
		$query->orWhere('token_code', $code);
		$result = $query->get();
		return $result->getRowArray();
	}

	public function updateToken($data, $email)
	{
		$query  = $this->db->table($this->table);
		$query->where('email', $email);
		return $query->update($data);
	}

	public function deleteToken($email)
	{
		$query  = $this->db->table($this->table);
		$query->where('email', $email);
		return $query->delete();
	}
}
