<?php

namespace App\Models;

use CodeIgniter\Model;


class Auth extends Model
{

	protected $table                = 'tb_login';
	protected $primaryKey           = 'login_id';
	protected $allowedFields        = ['login_id', 'email', 'password', 'role_id', 'status'];

	public function insertData($data)
	{
		$query  = $this->db->table($this->table);
		return $query->insert($data);
	}

	public function updateLogin($data, $email)
	{
		$query  = $this->db->table($this->table);
		$query->where('email', $email);
		return $query->update($data);
	}

	public function deleteLogin_email($email)
	{
		$query  = $this->db->table($this->table);
		$query->where('email', $email);
		return $query->delete();
	}
}
